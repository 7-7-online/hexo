---
title: MongoDB常用操作
date: 2024-03-06 15:33:38
tags: [MongoDB]
categories: 
- MongoDB
---


## MongoDB常用操作命令大全

### 一. 基本概念

| SQL术语/概念 | MongoDB术语/概念  |  解释/说明  |
| :----: | :-----: | :------: |
|  database  |  database     | 数据库         |
|  table     |  collection   | 数据库表/集合   |
|  row       |  document     | 数据记录行/文档  |
|  column    |  field        | 数据字段/域     |
|  index     |  index        | 索引           |
|  table joins  |     | 表连接,MongoDB不支持  |
|  primary key  |  primary key   | 主键,MongoDB自动将_id字段设置为主键  |

### 二. 数据库常用命令
##### 1、Help查看命令提示
##### 2、切换/创建数据库
```
use yourDB;		//当创建一个集合(table)的时候会自动创建当前数据库
```
##### 3、查询所有数据库
```
show dbs;
```
##### 4、删除当前使用数据库
```
use
db.dropDatabase();
```
##### 5.从指定主机上克隆数据库
```
db.cloneDatabase(“127.0.0.1”);	//将指定机器上的数据库的数据克隆到当前数据库
```
##### 6.从指定的机器上复制指定数据库数据到某个数据库
```
db.copyDatabase("mydb", "temp", "127.0.0.1"); 	//将本机的mydb的数据复制到temp数据库中
```
##### 7、修复当前数据库
```
db.repairDatabase();
```
##### 8、查看当前使用的数据库
```
db.getName();		//
db; db和getName方法是一样的效果，都可以查询当前使用的数据库
```
##### 9、显示当前db状态
```
db.stats();
```
##### 10、当前db版本
```
db.version();
```
##### 11、查看当前db的链接机器地址
```
db.getMongo();
```
### 三、Collection聚集集合 
```

```
##### 