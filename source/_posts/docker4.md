---
title: Docker-Compose安装卸载
date: 2024-01-04 21:47:16
tags: [Docker-Compose]
categories: 
- docker
---
> 一、Docker-Compose安装

#### 1.运行以下命令以下载Docker Compose的当前稳定版本：
```shell
 sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```
要安装其他版本的Compose，请替换1.26.0 为要使用的Compose版本。


下载Docker-Compose出现Failed to connect to github.com port 443: Connection refused问题
```
curl -L https://get.daocloud.io/docker/compose/releases/download/1.25.4/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
```

#### 2.将可执行权限应用于二进制文件：
```shell
sudo chmod +x /usr/local/bin/docker-compose
```
#### 3.查看版本 :
```shell
docker-compose version
```

> 二、Docker-Compose卸载

如果是二进制包方式安装的，删除二进制文件即可：
```shell
sudo rm /usr/local/bin/docker-compose
```
