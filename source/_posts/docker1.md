---
title: Docker安装及命令
date: 2024-01-04 21:10:30
tags: [linux,docker]
---
> 一、安装
```
1.更新yum
yum update
2.安装需要的软件包
yum -y install yum-utils device-mapper-persistent-data lvm2
3.配置docker源地址
yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
4.安装docker
yum install -y docker-ce
5.查看docker版本
docker -v

```
> 二、配置镜像加速器
```
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://myhhtv4l.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```
阿里云搜索镜像[加速器](https://blog.csdn.net/weixin_72926030/article/details/134206965)，将命令粘贴到Linux

> 三、docker服务命令
```
1.启动docker
systemctl start docker
2.查看docker状态
systemctl status docker
3.重启docker
systemctl restart docker
4.停止docker
systemctl stop docker
5.开机启动docker
systemctl enable docker

```
> 四、镜像命令
```
1.查看docker镜像
docker images
2.搜索镜像（以redis为例）
docker search redis
3.下载镜像
docker pull redis（下载最新版本latest）
docker pull redis:3.2（下载指定版本）
4.删除镜像
docker rmi IMAGE_ID（镜像id）
5.查看所有镜像ID
docker images -q
6.删除所有镜像（酌情使用）
docker rmi `docker images -q`
7.登录账户
docker login
```
查看某个镜像的版本号，可以前往docker官网：[https://hub.docker.com/](https://hub.docker.com/)
> 五、容器命令
```
1.查看正在运行的容器
docker ps
2.查看所有容器
docker ps -a
3.创建并启动容器
docker run 参数
-i:保持容器运行。通常与-t同时使用。加入it这两个参数后，容器创建后自动进入容器中，退出容器后，容器自动关闭。
-t:为容器重新输入一个伪输入终端，通常与-i同时使用。
-d:以守护（后台）模式运行容器。创建一个容器在后台运行，通常需要docker exec 进入容器。退出后，容器不会关闭。
-it:创建的容器一般称为交互式容器，-id创建的容器一般称为守护式容器。
--name:为创建的容器命名。
4.关闭容器
docker stop 容器名
5.删除容器
docker rm 容器id
docker rm 容器name
docker rm `docker ps -aq`（删除所有容器，运行的容器不能删除）
6.查看容器信息
docker inspect 容器name
7.进入容器
docker exec 容器名 -it /bin/bash

```
> 六、[docker-compose](http://weibo.com/u/5967841127)安装
```
sudo curl -L https://github.com/docker/compose/releases/download/1.27.4/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

检查是否安装成功
docker-compose --version

```
