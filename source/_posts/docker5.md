---
title: Docker-Compose模板文件
date: 2024-01-04 21:47:16
tags: [Docker-Compose]
categories: 
- docker
---
> 一、Docker-Compose.yml结构

docker-compose.yml文件包含：version、services、networks、volumes 、ports等配制组成
* version 定义了版本信息
  * 这个定义关乎与docker的兼容性
  * Compose 文件格式有3个版本,分别为1, 2.x 和 3.x
  * 目前主流的为 3.x 其支持 docker 1.13.0 及其以上的版本
  * | Compose 配置文件版本 | Docker 版本  |
    | :----: | :-----: |
    |  3.8  |  19.03.0+  |
    |  3.7  |  18.06.0+  |
    |  3.6  |  18.02.0+  |
    |  3.5  |  17.12.0+  |
    |  3.4  |  17.09.0+  |
    |  3.3  |  17.06.0+  |
    |  3.2  |  17.04.0+  |
    |  3.1  |  1.13.1+ |
    |  3.0  |  1.13.0+  |
    |  2.4  |  17.12.0+  |
    |  2.3  |  17.06.0+  |
    |  2.2  |  1.13.0+  |
    |  2.1  |  1.12.0+  |
    |  2.0  |  1.10.0+  |
    |  1.0  |  1.9.1+  |
    
* services主要用来定义各个容器（每个容器的配置）
* networks定义了网络信息，提供给 services 中的 具体容器使用，类似于命令行的 docker network create
* volumes卷挂载路径设置，设置宿主机路径:容器中路径
* ports 建立宿主机和容器之间的端口映射关系

> 二、Docker-Compose模板文件


```
# 指定配置文件的版本号
version: "3.8"

# 定义服务，可以多个
services:
	# 服务名称cache
    cache:
    　# 标明构建的镜像
      image: redis:6.0.5
      # 生成的容器名称
      container_name: my_redis
      # 指定网络
      networks:
          - mynetwork
      # 指定容器中需要挂载的文件
      volumes:
          - ./redis/redis.conf:/usr/local/etc/redis/redis.conf:ro
      # 容器启动后执行命令
      command: ["redis-server", "/usr/local/etc/redis/redis.conf",start.sh']
      # 宿主机与容器端口映射
      ports: # 指定端口映射，HOST:Container
          - "6379" # 指定容器的端口6379，宿主机会随机映射端口
          - "8080:80"  # 宿主机端口8080，对应容器80
      depends_on:
         - mysql #依赖容器启动之后再启动
         - redis
      # 暴露端口给-link或处于同一网络的容器，不暴露给宿主机
      expose: ["3000"]
      # 挂断自动重新启动
      restart: always
      # 指定容器的环境变量
      environment:
        - TZ=Asia/Shanghai # 设置容器时区与宿主机保持一致
        # - RUOYI_ENV=development 或 RUOYI_ENV: development
      env_file: # 从文件中获取环境变量，可以指定一个或多个文件，其优先级低于environment指定的环境变量
        - /opt/runtime_opts.env # 绝对路径
        - ./common.env # 相对路径，相对当前 docker-compose.yml 文件所在目录
        - ./apps/web.env # 相对路径，相对当前 docker-compose.yml 文件所在目录
      # 指定容器运行的用户为root
          user:
            root 
    # 指定服务名称
    mysql:
      # 指定服务使用的镜像
      image: mysql
      # 指定容器名称
      container_name: mysql
      # 指定服务运行的端口
      ports:
        - 3306:3306
      # 指定容器中需要挂载的文件
      volumes:
        - /etc/localtime:/etc/localtime
        - /data/mysql/log:/var/log/mysql
        - /data/mysql/data:/var/lib/mysql
        - /data/mysql/mysql-files:/var/lib/mysql-files
        - /data/mysql/conf:/etc/mysql
      # 挂断自动重新启动
      restart: always
       连接其他容器的服务
      links:
        - db:database #可以以database为域名访问服务名称为db的容器
      # 指定容器的环境变量
      environment:
        - TZ=Asia/Shanghai # 设置容器时区与宿主机保持一致
        - MYSQL_ROOT_PASSWORD=xiujingmysql. # 设置root密码
      # 指定容器运行的用户为root
      user:
        root   
      # 指定服务名称
      mongo:
        # 指定服务使用的镜像
        image: mongo
        # 指定容器名称
        container_name: mongo
        # 指定服务运行的端口
        ports:
          - 27017:27017
        # 指定容器中需要挂载的文件
        volumes:
          - /etc/localtime:/etc/localtime
          - /data/mongodb/db:/data/db
          - /data/mongodb/configdb:/data/configdb
          - /data/mongodb/initdb:/docker-entrypoint-initdb.d      
        # 挂断自动重新启动
        restart: always
        # 指定容器的环境变量
        environment:
          - TZ=Asia/Shanghai # 设置容器时区与宿主机保持一致
          - AUTH=yes
          - MONGO_INITDB_ROOT_USERNAME=admin #账号
          - MONGO_INITDB_ROOT_PASSWORD=admin #密码
    # 服务名称app 
    app:
      # 构建镜像
      build:
          # 指定上下文
          context: .
          # 指定构建脚本
          dockerfile: ./Dockerfile-multi-stage
      # 生成的容器名称
      container_name: my_hello
      # 指定网络
      networks:
          - mynetwork
      # 配置环境变量
      environment:
          - REDIS_HOST=my_redis
      # 指名容器依赖关系
      depends_on:
          - cache
      # 宿主机与容器端口映射
      ports:
          - "5000:5000"

# 网络配置，与services在同一层级，注意书写格式对齐
networks:
  #　标识自定义的网络，对应容器中指定的网络的名称
  mynetwork:
  　# 在容器网路中展示的名称
    name: my_network
    # 网络驱动类型
    driver: bridge

```

<font color=red>expose: ["3000"]</font> 暴露端口给-link或处于同一网络的容器，不暴露给宿主机
> 三、Docker-Compose模板文件示例