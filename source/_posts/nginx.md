---
title: Nginx 常用命令
date: 2023-12-25 12:11:47
tags: [nginx,linux]
categories:
  - nginx
---

> systemctl 操作命令
#### 使用service或systemctl来执行Nginx命令


```
systemctl属于Linux命令

# 启动
$ service nginx start
$ systemctl start nginx

# 停止
$ service nginx stop
$ systemctl stop nginx

# 重启
$ service nginx reload
$ systemctl reload nginx

# 强制重启
$ service nginx restart
$ systemctl restart nginx

# 查看nginx服务状态
$ service nginx status
$ systemctl status nginx

```


> Nginx 操作命令

```
# Nginx开始
$ sudo /etc/init.d/nginx start
如果运行成功，终端输出将显示以下内容：
$ Output
$ [ ok ] Starting nginx (via systemctl): nginx.service.

# Nginx重启
$ sudo /etc/init.d/nginx restart
或者，使用以下nginx -s命令：
$ sudo nginx -s restart

# Nginx停止
$ sudo /etc/init.d/nginx stop
或者，使用：
$ sudo nginx -s stop

# Nginx重新加载
$ sudo /etc/init.d/nginx reload
或者，您可以使用nginx -s命令将指令直接传递给Nginx：
$ sudo nginx -s reload

# Nginx退出
$ sudo nginx -s quit

```