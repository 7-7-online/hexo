---
title: SSH免密登录服务器
date: 2023-11-14 21:56:53
tags: [linux]
categories:
  - ssh
---
#### 1. 在本地机器生成SSH密钥对（如果还没有的话）
```
ssh-keygen -t rsa
```
一路回车，各种提示按默认不要改，生成的密钥对 `id_rsa（私钥）`,` id_rsa.pub（公钥）`，默认存储在 `~/.ssh`目录 下
#### 2. 将你的公钥复制到远程服务器上的`~/.ssh/authorized_keys`文件中：
```
ssh-copy-id user@remote_host
ssh-copy-id root@172.90.79.102
或者，通过复制将本地id_rsa.pub（公钥）内容复制道服务器的~/.ssh/authorized_keys文件中
```
其中`user`是你的远程服务器上的用户名，`remote_host`是远程服务器的地址

#### 3. 确保远程服务器上的`~/.ssh`目录和`~/.ssh/authorized_keys`文件的权限设置正确：
```
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys
```

[//]: # (将本地id_rsa.pub文件的内容追加到服务器的vim ~/.ssh/authorized_keys文件。)

[//]: # (若是要用root用户免密登录，则需要追加到服务的/root/authorized_keys文件。)

[//]: # (服务器对应目录中如果不能存在authorized_keys文件直接创建即可。)

[//]: # (authorized_keys需要是600的读写权限)

#### 4. 本地配置参数连接
```
# 同样在~/.ssh/目录下，添加一个配置文件
cd ~/.ssh/ 
vim config

```

```
# 可以设置多个
Host host0  # 远程主机别名
  HostName 192.168.63.8  # 远程主机ip
  User root  # 你在远程主机的用户名（可忽略）
  Port 22  # 端口
  IdentityFile ~/.ssh/id_ssh  # 你的ssh秘钥文件（可忽略）


Host host1  # 远程主机别名
  HostName 192.168.63.8  # 远程主机ip
  User root  # 你在远程主机的用户名（可忽略）
  Port 22  # 端口
  IdentityFile ~/.ssh/id_ssh  # 你的ssh秘钥文件（可忽略）

```
##### 这样直接通过别名就可以访问了
```
ssh host0 
ssh host1 
```
