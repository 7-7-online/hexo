---
title: go基础——基本类型
date: 2023-09-20 23:35:41
tags: [go]
categories: 
- 编程
---

### Go 支持的基本类型

* bool
* string
* 数字类型
  * int8, int16, int32, int64, int
  * uint8, uint16, uint32, uint64, uint
  * float32, float64
  * complex64, complex128
  * byte
  * rune
    * int： 在32位系统中，4个字节，在64位系统中， 8个字节
    * byte 是 uint8 的别名。
    * rune 是 int32 的别名。
* byte 等同于uint8，常用来处理ascii字符，一个byte可以表示一个0-255之间的十进制数，两个十六进制数，一个十六进制需要4位表示
* rune 等同于int32, 常用来处理unicode或utf-8字符，通常使用utf8.RuneCountInString(“中国”)来统计中文字符串长度等
  
### 基础语法（整型：uint、uint8、uint16、uint32、uint64、uintptr）

#### `整型`范围
```
Int8 - [-128 : 127]
Int16 - [-32768 : 32767]
Int32 - [-2147483648 : 2147483647]
Int64 - [-9223372036854775808 : 9223372036854775807]
```
#### 无符号整型范围
```
UInt8 - [0 : 255]
UInt16 - [0 : 65535]
UInt32 - [0 : 4294967295]
UInt64 - [0 : 18446744073709551615]
```
