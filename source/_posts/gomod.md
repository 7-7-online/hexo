---
title: Go Mod 常用操作详解
date: 2024-06-07 16:37:56
tags: [Go Mod]
categories:
  - go mod
---
### 一.GO 设置代理
#### 1.1 打开模块支持
```
go env -w GO111MODULE=on
```
#### 1.2 取消代理
```
go env -w GOPROXY=direct
```
#### 1.3 关闭包的有效性验证
```
go env -w GOSUMDB=off
```
#### 1.4 设置不走 proxy 的私有仓库或组，多个用逗号相隔（可选）
```
go env -w GOPRIVATE=git.mycompany.com,github.com/my/private
```
#### 1.5 设置代理
##### 1.5.1国内常用代理列表
| 提供者 | 地址  |
| :----: | :-----: |
|  官方全球代理  |  https://proxy.golang.com.cn   |
|  七牛云  |  	https://goproxy.cn   |
|  阿里云  |  https://mirrors.aliyun.com/gopr   |
|  GoCenter  |  https://gocenter.io   |
|  百度  |  https://goproxy.bj.bcebos.com/  |
###### “direct” 为特殊指示符，用于指示 Go 回源到模块版本的源地址去抓取(比如 GitHub 等)，当值列表中上一个 Go module proxy 返回 404 或 410 错误时，Go 自动尝试列表中的下一个，遇见 “direct” 时回源，遇见 EOF 时终止并抛出类似 “invalid version: unknown revision…” 的错误。
##### 1.5.2 官方全球代理
```
go env -w GOPROXY=https://proxy.golang.com.cn,direct
go env -w GOPROXY=https://goproxy.io,direct
go env -w GOSUMDB=gosum.io+ce6e7565+AY5qEHUk/qmHc5btzW45JVoENfazw8LielDsaI+lEbq6
go env -w GOSUMDB=sum.golang.google.cn
```

##### 1.5.3 七牛云
```
go env -w GOPROXY=https://goproxy.cn,direct
go env -w GOSUMDB=goproxy.cn/sumdb/sum.golang.org
```

##### 1.5.4 阿里云
```
go env -w GOPROXY=https://mirrors.aliyun.com/goproxy/,direct
# GOSUMDB 不支持
```

##### 1.5.6 GoCenter
```
go env -w GOPROXY=https://gocenter.io,direct
# 不支持 GOSUMDB
```

##### 1.5.7 百度
```
go env -w GOPROXY=https://goproxy.bj.bcebos.com/,direct
# 不支持 GOSUMDB
```

#### 1.7 查看Go的配置
```
$ go env
//以JSON格式输出
$ go env -json
```

### 二.设置 GO111MODULE
##### 要启用go module支持首先要设置环境变量GO111MODULE，通过它可以开启或关闭模块支持，它有三个可选值：off、on、auto，默认值是auto。GO111MODULE=off禁用模块支持，编译时会从GOPATH和vendor文件夹中查找包。GO111MODULE=on启用模块支持，编译时会忽略GOPATH和vendor文件夹，只根据 go.mod下载依赖.GO111MODULE=auto，当项目在$GOPATH/src外且项目根目录有go.mod文件时，开启模块支持。
#### 设置Go Model
```
      # 临时开启 Go modules 功能
      export GO111MODULE=on
      # 永久开启 Go modules 功能
      go env -w GO111MODULE=on
```


### 三.godep 基本使用
##### Go语言从v1.5开始开始引入vendor模式，如果项目目录下有vendor目录，那么go工具链会优先使用vendor内的包进行编译、测试等。
##### godep是一个通过vender模式实现的Go语言的第三方依赖管理工具，类似的还有由社区维护准官方包管理工具dep

#### 3.1 安装
##### 执行以下命令安装godep工具。
```
go get github.com/tools/godep
```


#### 3.2 基本命令
##### 安装好godep之后，在终端输入godep查看支持的所有命令。
```
godep save     将依赖项输出并复制到Godeps.json文件中
godep go       使用保存的依赖项运行go工具
godep get      下载并安装具有指定依赖项的包
godep path     打印依赖的GOPATH路径
godep restore  在GOPATH中拉取依赖的版本
godep update   更新选定的包或go版本
godep diff     显示当前和以前保存的依赖项集之间的差异
godep version  查看版本信息
```


#### 3.3 使用godep
##### 在项目目录下执行godep save命令，会在当前项目中创建Godeps和vender两个文件夹。
      
##### 其中Godeps文件夹下有一个Godeps.json的文件，里面记录了项目所依赖的包信息。 vender文件夹下是项目依赖的包的源代码文件。

#### 3.4 vender机制
##### 例如查找项目的某个依赖包，首先会在项目根目录下的vender文件夹中查找，如果没有找到就会去$GOAPTH/src目录下查找。



### 四.Go module 常用操作
#### 4.1初始化项目
```
基于当前项目创建一个 Go Module，通常有如下几个步骤：

通过 go mod init 项目名 创建 go.mod 文件，将当前项目变为一个 Go Module；
通过 go mod tidy 命令自动更新当前 module 的依赖信息；
执行 go build，执行新 module 的构建。
然后会生成两个文件go.mod和go.sum.
```


### go.mod文件记录了项目所有的依赖信息，其结构大致如下：
```
module dome
go 1.18
require (
	github.com/google/uuid v1.3.0
	github.com/sirupsen/logrus v1.9.0
)

require (
	github.com/kr/fs v0.1.0 // indirect 
	github.com/kr/pretty v0.3.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rogpeppe/go-internal v1.9.0 // indirect
	github.com/tools/godep v0.0.0-20180126220526-ce0bfadeb516 // indirect
	golang.org/x/sys v0.3.0 // indirect
	golang.org/x/tools v0.4.0 // indirect
)
```
* module用来模块名称
* require用来定义依赖包及版本
* exclude 禁止依赖包列表，不下载和引用哪些包(仅在当前模块为主模块时生效)
* replace 替换依赖包列表和引用路径(仅在当前模块为主模块时生效)
* indirect 表示这个库是间接引用进来的。

#### replace
##### 在国内访问http://golang.org/x的各个包都需要翻墙，你可以在go.mod中使用replace替换成github上对应的库。
```
replace (
	golang.org/x/net => github.com/golang/net latest
	golang.org/x/tools => github.com/golang/tools latest
	golang.org/x/crypto => github.com/golang/crypto latest
	golang.org/x/sys => github.com/golang/sys latest
	golang.org/x/text => github.com/golang/text latest
	golang.org/x/sync => github.com/golang/sync latest
)
```


#### 4.3 删除未使用的依赖
##### 可以用 go mod tidy 命令来清除这些没用到的依赖项：
```
go mod tidy
```
##### go mod tidy会自动分析源码依赖，而且将不再使用的依赖从 go.mod 和 go.sum 中移除。


### 五.Go module 常用命令总结：
#### 5.1 go mod命令
##### 常用的go mod命令如下：
```
go mod download    下载依赖的module到本地cache（默认为$GOPATH/pkg/mod目录）
go mod edit        编辑go.mod文件
go mod graph       打印模块依赖图
go mod init        初始化当前文件夹, 创建go.mod文件
go mod tidy        增加缺少的module，删除无用的module
go mod vendor      将依赖复制到vendor下
go mod verify      校验依赖
go mod why         解释为什么需要依赖
```