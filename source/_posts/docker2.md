---
title: Dockerfile镜像配置
date: 2024-04-03 23:42:27
tags: [Docker-Dockerfile]
categories: 
- docker
---
## <font color=red>DockerFile 的常用指令</font>
![玖涯](/img/docker/DockerFile.png)
## <font color=red>指令介绍</font>

[//]: # (![玖涯]&#40;/img/docker/DockerFile2.png&#41;)
* FROM：指定基础镜像
* RUN：在镜像上执行命令
* CMD：容器启动时默认执行的命令
* ENTRYPOINT：配置容器启动时运行的命令
* LABEL：为镜像添加元数据标签
* EXPOSE：声明运行容器时应该暴露的端口
* ENV：设置环境变量
* ADD：将本地文件添加到镜像中
* COPY：将本地文件复制到镜像中
* WORKDIR：设置工作目录
* USER：指定运行容器时的用户
* VOLUME：创建一个可以从本机或其他容器挂载的挂载点
* ONBUILD：当一个被继承的Dockerfile中使用，其父镜像在被pulled时触发


## <font color=red>指令语法</font>
* <font color=red>FROM 指定基础镜像</font>
```
FROM <image>
# 或
FROM <image>:<tag>
# 或
FROM <image>:<digest>
```

* <font color=red>RUN 执行命令</font>
    * <font color=red>RUN</font> 指令在镜像的构建过程中执行特定的命令，并生成一个中间镜像。
    * 语法格式如下（有两种格式）：
       * <font color=red>shell</font> 格式：
       ```
         RUN <命令行命令>
         # <命令行命令> 等同于，在终端操作的 shell 命令。
       ```
       * <font color=red>exec</font> 格式：
       ```
        RUN ["可执行文件", "参数1", "参数2"]
        # 例如：
        # RUN ["./test.php", "dev", "offline"] 等价于 RUN ./test.php dev offline

       ```
* <font color=red>COPY 复制文件</font>
    * 复制指令，从上下文目录中复制文件或者目录到容器里指定路径。
    * 语法格式如下：
    ```
     COPY [--chown=<user>:<group>] <源路径1>...  <目标路径>
     COPY [--chown=<user>:<group>] ["<源路径1>",...  "<目标路径>"]
     
     COPY hom* /mydir/
     COPY hom?.txt /mydir/
     
     <目标路径>：容器内的指定路径，该路径不用事先建好，路径不存在的话，会自动创建。
  
    ```
    * <font color=red><源路径></font> 可以是多个，甚至可以是通配符，其通配符规则要满足 Go 的 filepath.Match 规则，如
    ```
    COPY hom* /mydir/
    COPY hom?.txt /mydir/

    ```
    * <font color=red><目标路径></font> 可以是容器内的绝对路径，也可以是相对于工作目录的相对路径（工作目录可以用 WORKDIR 指令来指定）。目标路径不需要事先创建，如果目录不存在会在复制文件前先行创建缺失目录。
* <font color=red>ADD 更高级的复制文件</font>
* <font color=red>CMD</font>
    * 类似于 <font color=red>RUN</font> 指令，用于运行程序，但二者运行的时间点不同
       * <font color=red>CMD</font> 在 <font color=red>docker run</font> 时运行，在构建时不进行任何操作。
       * <font color=red>RUN</font> 是在 <font color=red>docker build</font>，并生成一个新的镜像。
    * 作用：为启动的容器指定默认要运行的程序，程序运行结束，容器也就结束。CMD 指令指定的程序可被 docker run 命令行参数中指定要运行的程序所覆盖。
    * 注意：如果 Dockerfile 中如果存在多个 CMD 指令，仅最后一个生效
    * 语法格式如下（有三种格式）：
    ```
     CMD <shell 命令> 
     CMD ["<可执行文件或命令>","<param1>","<param2>",...]
     CMD ["<param1>","<param2>",...]  # 该写法是为 ENTRYPOINT 指令指定的程序提供默认参数

    ```
* <font color=red>ENTRYPOINT</font>
* <font color=red>ENV 设置环境变量</font>
* <font color=red>ARG</font>
* <font color=red>VOLUME 定义匿名卷</font>
    * 定义匿名数据卷。在启动容器时忘记挂载数据卷，会自动挂载到匿名卷
    * 作用：
        * 避免重要的数据，因容器重启而丢失，这是非常致命的。
        * 避免容器不断变大。
    * 语法格式如下：
    ```
     VOLUME ["<路径1>", "<路径2>"...]
     VOLUME <路径>

    ```
    * 在启动容器 docker run 的时候，我们可以通过 -v 参数修改挂载点
* <font color=red>EXPOSE</font>
    * 为构建的镜像设置监听端口，使容器在运行时监听（仅仅只是声明端口）。
    * 作用：
        * 帮助镜像使用者理解这个镜像服务的守护端口，以方便配置映射。
        * 在运行时使用随机端口映射时，也就是 <font color=red>docker run -P</font> 时，会自动随机映射 <font color=red>EXPOSE</font> 的端口。
    * 语法格式如下：
     ```
     EXPOSE <port> [<port>...]

     ```
    * EXPOSE 指令并不会让容器监听 host 的端口，如果需要，需要在 docker run 时使用 -p、-P 参数来发布容器端口到 host 的某个端口上
* <font color=red>WORKDIR 指定工作目录</font>
    * 指定工作目录。用 <font color=red>WORKDIR</font> 指定的工作目录，会在构建镜像的每一层中都存在。以后各层的当前目录就被改为指定的目录，如该目录不存在，<font color=red>WORKDIR</font> 会帮你建立目录
    * <font color=red>docker build</font> 构建镜像过程中的，每一个 <font color=red>RUN</font> 命令都是新建的一层。只有通过 <font color=red>WORKDIR</font> 创建的目录才会一直存在。
    * 语法格式如下：
     ```
      WORKDIR <工作目录路径>

     ```
    * 通过 <font color=red>WORKDIR</font> 设置工作目录后，<font color=red>Dockerfile</font> 中其后的命令 RUN、CMD、ENTRYPOINT、ADD、COPY 等命令都会在该目录下执行。
    * 例如，使用 <font color=red>WORKDIR</font> 设置工作目录：
     ```
      WORKDIR /a
      WORKDIR b
      WORKDIR c
      RUN pwd

     ```
    * 在以上示例中，pwd 最终将会在 /a/b/c 目录中执行。在使用 docker run 运行容器时，可以通过 -w 参数覆盖构建时所设置的工作目录。
  
* <font color=red>HEALTHCHECK</font>
* <font color=red>ONBUILD</font>
* <font color=red>LABEL</font>
* <font color=red>STOPSIGNAL</font>
* <font color=red>SHELL</font>