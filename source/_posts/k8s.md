---
title: k8s命令
date: 2023-08-13 21:26:47
tags:
- 运维
- 表格
- 表单验证
categories: 
- web前端
---
#### 构建k8s步骤
##### 关闭防火墙
    sudo systemctl status firewalld.service
    sudo systemctl status firewalld.service
    sudo firewall-cmd --state
    sudo sed -ri 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config # 禁用Security-Enhanced Linux
##### 同步时钟
    yum install -y ntp
    ntpdate time1.aliyun.com
    crontab -e
##### 关闭swap
   k8s官方要求使用真实的物理内存，而不是swap的虚拟内存，据说k8s v1.25后不需要再关闭swap
```    居家发的发发发
sudo swapoff -a # 临时关闭，立即生效
sudo vim /etc/fstab
#/dev/mapper/centos-swap swap                    swap    defaults        0 0  # 注释掉该行，机器重启后swap仍是关闭状态，永久生效
sudo free -h  # 检查swap是否关闭
              total        used        free      shared  buff/cache   available
Mem:           7.6G        1.0G        456M        448M        6.2G        5.3G
Swap:            0B          0B          0B
```
##### 开启内核路由转发
如果不开启，发布应用后，无法访问应用
```
$ sudo vim /etc/sysctl.conf
net.ipv4.ip_forward=1
$ sudo sysctl -p  # 使之生效
```

##### 安装docker
参考docker官网安装文档
https://docs.docker.com/engine/install/
```
# 查看docker客户端和服务端版本信息
[root@localhost ~]# docker version
# 查看docker更详细的信息
[root@localhost ~]# docker info
# 查看docker命令帮助
[root@localhost ~]# docker 命令 --help
# 退出已连接的docker命令， 如果docker无程序在执行，退出后会停止运行容器
[root@localhost ~]# exit      
# 退出已连接的docker快捷键, 无论docker有无程序在执行，退出后均不会停止运行容器
Ctrl + P + Q
```
###### 首先卸载旧的Docker
    yum remove docker \
    docker-client \
    docker-client-latest \
    docker-common \
    docker-latest \
    docker-latest-logrotate \
    docker-logrotate \
    docker-engine

##### 安装docker
1. 准备安装环境
```
yum install -y yum-utils
```
2. 设置镜像仓库为阿里云
```
yum-config-manager \
    --add-repo \
    http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```
2. 设置镜像仓库为清华大学源
```
yum-config-manager --add-repo https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/centos/docker-ce.repo
```
```
yum clean all
yum makecache
yum install docker-ce docker-ce-cli containerd.io #安装docker
```



###### docker的基础命令
1. 配置镜像加速器
    ```
    sudo mkdir -p /etc/docker
    sudo tee /etc/docker/daemon.json <<-'EOF'
    {
      "registry-mirrors": ["https://myhhtv4l.mirror.aliyuncs.com"]
    }
    EOF
    
    sudo systemctl daemon-reload
    sudo systemctl restart docker
    ```
2. 启动docker
    ```
    systemctl start docker
    ```
2. 关闭docker
    ```
    systemctl stop docker
    ```
3. 重启docker
   ```
   systemctl restart docker 
   ```
4. 设置docker开机自启动
   ```
   systemctl enable docker
   ```
1. 查看docker运行状态（显示绿色代表正常启动）
    ```
    systemctl status docker 
    ```
2. 忘记了某些命令的使用可以查看
    ```
    docker --help
    ```
4. 假如我们启动镜像的时候忘记命令的使用 
    ```
    docker  run  --help
    ```
5. 卸载docker
```
# 卸载依耐
[root@localhost ~]# yum remove docker-ce docker-ce-cli containerd.io
# 删除资源
[root@localhost ~]# rm -rf /var/lib/docker

```

###### docker镜像
1. 查找镜像
    ```
    docker search <镜像id/镜像名>
    ```
1. 查看docker镜像列表
    ```
    docker  images
    ```
2. 单独搜索镜像
    ```
    docker  images  镜像名 
    ```
3. 下载拉取镜像 不加tag(版本号) 即拉取docker仓库中 该镜像的最新版本latest 加:tag 则是拉取指定版本
    ```
    docker pull 镜像名 # 最新版本
    docker pull 镜像名:tag 
    ```
4. 删除没有用的镜像
    ```
    #删除一个
    docker rmi -f 镜像名/镜像ID
    
    #删除多个 其镜像ID或镜像用用空格隔开即可 
    docker rmi -f 镜像名/镜像ID 镜像名/镜像ID 镜像名/镜像ID
    
    #删除全部镜像  -a 意思为显示全部, -q 意思为只显示ID
    docker rmi -f $(docker images -aq)
    ```
5. 强制删除
    ```
    docker image rm 镜像名称/镜像ID
    ```

###### docker容器命令
查看正在运行的容器
```
docker ps
```
查看所有容器包括正在运行和停掉的容器 
```
# 查看全部容器
docker ps -a
# 查看全部容器和总文件大小
docker ps -a -s
# 显示最近创建的3个容器
docker ps -n 3
# 显示最近创建的容器
docker ps -l
docker ps -a -l
# 只显示镜像id
docker ps -q
#显示完整镜像id
docker ps --no-trunc

```
运行一个容器
```
docker start 容器id
```
然后查看运行的容器  这里可以看到mysql：5.7容器正在运行
进入容器（新终端）  这里的5f39bcf1dbad是容器ID
```
docker exec -it 容器id/容器名
```
进入容器
```
docker attach 容器id/容器名
```
启动容器
```
docker start 容器id
```
重启容器
```
docker restart 容器id
```
停止容器
```
docker stop 容器id
```
获取容器日志
```
docker logs 容器id
```
查看容器信息
```
docker inspect <容器id>
```
容器自启动
```
docker update --restart=always <容器id/容器名>
```
删除容器  首先要停止运行的容器
```
# 先停止咱之前运行的 mysql5.7 容器 
docker stop 容器名/容器ID
强制删除容器 db01、db02
[root@localhost ~]# docker rm -f db01 db02

移除容器 nginx01 对容器 db01 的连接，连接名 db
[root@localhost ~]# docker rm -l db 

删除容器 nginx01, 并删除容器挂载的数据卷
[root@localhost ~]# docker rm -v nginx01

删除所有已经停止的容器
[root@localhost ~]# docker rm $(docker ps -aq)
```

删除所有已经停止的容器
```
docker rm $(docker ps -aq)
```
docker pause :暂停容器中所有的进程
```
暂停数据库容器db01提供服务
[root@localhost ~]# docker pause db01
```

docker create ：创建一个新的容器但不启动它
```
使用docker镜像nginx:latest创建一个容器,并将容器命名为mycon
[root@localhost ~]# docker create --name mycon nginx:latest
```
容器和本地文件系统之间拷贝文件/文件夹
```
docker cp 本地文件的路径 <容器id>:容器路径
```
容器端口与服务器端口映射
退出容器就是

```
创建容器常用的参数说明：

创建容器命令：docker run

 -i：表示运行容器

 -t：表示容器启动后会进入其命令行。加入这两个参数后，容器创建就能登录进去。即分配一个伪终端。

 --name :为创建的容器命名。

 -v：表示目录映射关系（前者是宿主机目录，后者是映射到宿主机上的目录），可以使用多个－v做多个目录或文件映射。注意：最好做目录映射，在宿主机上做修改，然后共享到容器上。

 -d：在run后面加上-d参数,则会创建一个守护式容器在后台运行（这样创建容器后不会自动登录容器，如果只加-i -t两个参数，创建后就会自动进去容器）。

 -p：(小写P)表示端口映射，前者是宿主机端口，后者是容器内的映射端口。可以使用多个-p做多个端口映射
 
 -P: (大写P)随机端口映射，容器内部端口随机映射到主机的端口
 
 –dns 8.8.8.8: 指定容器使用的DNS服务器，默认和宿主一致；
 
 -e username="ritchie": 设置环境变量
 
 –cpuset=“0-2” or --cpuset=“0,1,2”: 绑定容器到指定CPU运行；
 
 -m :设置容器使用内存最大值；
 
 –net=“bridge”: 指定容器的网络连接类型，支持 bridge/host/none/container: 四种类型；
 
 –link=[]: 添加链接到另一个容器
 
 –expose=[]: 开放一个端口或一组端口；
```


##### 常用容器启动命令
1. Nginx
```
docker run  --name nginx -p 80:80 -p 443:443   -v /opt/nginx/html:/usr/share/nginx/html  -v /opt/nginx/conf.d/:/etc/nginx/conf.d/  -v /opt/nginx/logs:/var/log/nginx -d --restart=always nginx

```
1. Redis
```
docker run -p 6379:6379 --name redis -v /opt/redis/redis.conf:/etc/redis/redis.conf -v /opt/redis/data:/data  redis-server /etc/redis/redis.conf -d --restart=always redis

```
1. Mysql
```
docker run -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root -v /opt/mysql/data:/var/lib/mysql -v /opt/mysql/conf:/etc/mysql/conf.d --name mysql -d --restart=always mysql

```
1. Nacos
```
docker run -d --name nacos -p 8848:8848 -p 9848:9848 -p 9849:9849 -e MODE=standalone -v /opt/nacos/logs/:/home/nacos/logs -v /opt/nacos/conf/:/home/nacos/conf/ --restart=always nacos

```
##### 安装rancher

```
sudo docker run -d --privileged --restart=unless-stopped --name rancher -p 9080:80 -p 9443:443  -v /var/lib/rancher/  -e CATTLE_AGENT_IMAGE="registry.cn-hangzhou.aliyuncs.com/rancher/rancher-agent:v2.4.2" registry.cn-hangzhou.aliyuncs.com/rancher/rancher:v2.4.2
```
##### 主要需要把镜像换成阿里镜像要不然拉取信息会失败-设置默认镜像仓库
```
UI导航到Settings，然后编辑system-default-registry
Value设置为registry.cn-hangzhou.aliyuncs.com
```
##### 第二次安装rancher，需要清除之前数据，要不然会安装失败
```
rm -rf /etc/ceph \
       /etc/cni \
       /etc/kubernetes \
       /opt/cni \
       /opt/rke \
       /run/secrets/kubernetes.io \
       /run/calico \
       /run/flannel \
       /var/lib/calico \
       /var/lib/etcd \
       /var/lib/cni \
       /var/lib/kubelet \
       /var/lib/rancher/rke/log \
       /var/log/containers \
       /var/log/pods \
       /var/run/calico
```
