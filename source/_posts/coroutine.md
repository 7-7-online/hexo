---
title: Go.初探 Goroutine 与 channel
date: 2023-11-30 23:49:54
tags: [go,协程]
categories: 
- 编程
- 并发
---

# 前言
Go 语言的 CSP 并发模型的实现包含两个主要组成部分：一个是 Goroutine，另一个是 channel。本文将会介绍它们的基本用法和注意事项。

# Goroutine
Goroutine 是 Go 应用的基本执行单元，它是一种轻量的用户级线程，其底层是通过 coroutine（协程）去实现的并发。众所周知，协程是一种运行在用户态的用户线程，因此 Goroutine 也是被调度于 Go 程序运行时。
## 基本用法
> 语法：go + 函数/方法
>
> 
> 通过 go 关键字 + 函数/方法 可以创建一个 Goroutine。

代码示例：
```
import (
   "fmt"
   "time"
)

func printGo() {
   fmt.Println("具名函数")
}

type G struct {
}

func (g G) g() {
   fmt.Println("方法")
}

func main() {
   // 基于具名函数创建 goroutine
   go printGo()
   // 基于方法创建 goroutine
   g := G{}
   go g.g()
   // 基于匿名函数创建 goroutine
   go func() {
      fmt.Println("匿名函数")
   }()
   // 基于闭包创建 goroutine
   i := 0
   go func() {
      i++
      fmt.Println("闭包")
   }()
   time.Sleep(time.Second) // 避免 main goroutine 结束后，其创建的 goroutine 来不及运行，因此在此休眠 1 秒
}

```
执行结果：



```
闭包
具名函数
方法
匿名函数

```

当多个 Goroutine 存在时，它们的执行顺序是不固定的。因此每次打印的结果都不相同。



由代码可知，通过 go 关键字，我们可以基于 具名函数 / 方法 创建 goroutine，也可以基于 匿名函数 / 闭包 创建 goroutine。



那么 Goroutine 是如何退出的呢？正常情况下，只要 Goroutine 函数执行结束，或者执行返回，意味着 Goroutine 的退出。如果 Goroutine 的函数或方法有返回值，在 Goroutine 退出时会将其忽略

## channel
channel 在 Go 并发模型中扮演者重要的角色。它可以用于实现 Goroutine 间的通信，也可以用来实现 Goroutine 间的同步。
### channel 的基本操作
channel 是一种复合数据类型，声明时需要指定 channel 里元素的类型。
> 声明语法：var 变量名 chan 类型
通过上述代码声明一个元素类型为 string 的 channel，其只能存放 string 类型的元素。channel 是引用类型，必须初始化才能写入数据，通过 make 的方式初始化。
```
import (
   "fmt"
)

func main() {
   var ch chan string
   ch = make(chan string, 1)
   // 打印 chan 的地址
   fmt.Println(ch)
   // 向 ch 发送 "Go" 数据
   ch <- "Go"
   // 从 ch 中接收数据
   s := <-ch
   fmt.Println(s) // Go
}

```
通过 ch <- xxx 可以向 channel 变量 ch 发送数据，通过 x := <- ch 可以从 channel 变量 ch 中接收数据。

### 带缓冲 channel 与无缓冲 channel
如果初始化 channel 时，不指定容量时，则创建的是一个无缓冲的 channel：
```
ch := make(chan string)

```

一文初探 Goroutine 与 channel
作者：陈明勇
2023-04-21
广东
本文字数：2665 字

阅读完需：约 9 分钟

一文初探 Goroutine 与 channel
作者：陈明勇

专注分享后端知识，如果本文对你有帮助，不妨点个赞，如果你是 Go 语言初学者，不妨点个关注，一起成长一起进步，如果本文有错误的地方，欢迎指出！

前言
Go 语言的 CSP 并发模型的实现包含两个主要组成部分：一个是 Goroutine，另一个是 channel。本文将会介绍它们的基本用法和注意事项。

Goroutine
Goroutine 是 Go 应用的基本执行单元，它是一种轻量的用户级线程，其底层是通过 coroutine（协程）去实现的并发。众所周知，协程是一种运行在用户态的用户线程，因此 Goroutine 也是被调度于 Go 程序运行时。

基本用法


语法：go + 函数/方法



通过 go 关键字 + 函数/方法 可以创建一个 Goroutine。



代码示例：



import (
   "fmt"
   "time"
)

func printGo() {
   fmt.Println("具名函数")
}

type G struct {
}

func (g G) g() {
   fmt.Println("方法")
}

func main() {
   // 基于具名函数创建 goroutine
   go printGo()
   // 基于方法创建 goroutine
   g := G{}
   go g.g()
   // 基于匿名函数创建 goroutine
   go func() {
      fmt.Println("匿名函数")
   }()
   // 基于闭包创建 goroutine
   i := 0
   go func() {
      i++
      fmt.Println("闭包")
   }()
   time.Sleep(time.Second) // 避免 main goroutine 结束后，其创建的 goroutine 来不及运行，因此在此休眠 1 秒
}



执行结果：

闭包
具名函数
方法
匿名函数



当多个 Goroutine 存在时，它们的执行顺序是不固定的。因此每次打印的结果都不相同。



由代码可知，通过 go 关键字，我们可以基于 具名函数 / 方法 创建 goroutine，也可以基于 匿名函数 / 闭包 创建 goroutine。



那么 Goroutine 是如何退出的呢？正常情况下，只要 Goroutine 函数执行结束，或者执行返回，意味着 Goroutine 的退出。如果 Goroutine 的函数或方法有返回值，在 Goroutine 退出时会将其忽略。

channel
channel 在 Go 并发模型中扮演者重要的角色。它可以用于实现 Goroutine 间的通信，也可以用来实现 Goroutine 间的同步。

channel 的基本操作
channel 是一种复合数据类型，声明时需要指定 channel 里元素的类型。



声明语法：var 变量名 chan 类型



通过上述代码声明一个元素类型为 string 的 channel，其只能存放 string 类型的元素。channel 是引用类型，必须初始化才能写入数据，通过 make 的方式初始化。
```
import (
   "fmt"
)

func main() {
   var ch chan string
   ch = make(chan string, 1)
   // 打印 chan 的地址
   fmt.Println(ch)
   // 向 ch 发送 "Go" 数据
   ch <- "Go"
   // 从 ch 中接收数据
   s := <-ch
   fmt.Println(s) // Go
}

```

通过 ch <- xxx 可以向 channel 变量 ch 发送数据，通过 x := <- ch 可以从 channel 变量 ch 中接收数据。

带缓冲 channel 与无缓冲 channel
如果初始化 channel 时，不指定容量时，则创建的是一个无缓冲的 channel：
```
ch := make(chan string)
```


无缓冲的 channel 的发送与接收操作是同步的，在执行发送操作之后，对应 Goroutine 将会阻塞，直到有另一个 Goroutine 去执行接收操作，反之亦然。如果将发送操作和执行操作放在同一个 Goroutine 下进行，会发生什么操作呢？看看下述代码：



```
import (
   "fmt"
)

func main() {
   ch := make(chan int)
   // 发送数据
   ch <- 1 // fatal error: all goroutines are asleep - deadlock!
   // 接收数据
   n := <-ch
   fmt.Println(n)
}

```


程序运行之后，会在 ch <- 处得到 fatal error，提示所有的 Goroutine 处于休眠状态，也就是死锁了。为避免这种情况，我们需要将 channel 的发送操作和接收操作放到不同的 Goroutine 中执行。

```
import (
   "fmt"
)

func main() {
   ch := make(chan int)
   go func() {
      // 发送数据
      ch <- 1
   }()
   // 接收数据
   n := <-ch
   fmt.Println(n) // 1
}

```




由上述例子可以得出结论：无缓冲 channel 的发送与接收操作，一定要放在两个不同的 Goroutine 中进行，否则会发生 deadlock 形象。



如果指定容量，则创建的是一个带缓冲的 channel：
```
由上述例子可以得出结论：无缓冲 channel 的发送与接收操作，一定要放在两个不同的 Goroutine 中进行，否则会发生 deadlock 形象。



如果指定容量，则创建的是一个带缓冲的 channel：
```
有缓冲的 channel 与无缓冲的 chennel 有所区别，执行发送操作时，只要 channel 的缓冲区未满，Goroutine 不会挂起，直到缓冲区满时，再向 channel 执行发送操作，才会导致 Goroutine 挂起。代码示例：


```
func main() {
   ch := make(chan int, 1)
   // 发送数据
   ch <- 1

   ch <- 2 // fatal error: all goroutines are asleep - deadlock!
}

```

### 声明 channel 的只发送类型和只接收类型
既能发送又能接收的 channel
* 既能发送又能接收的 channel
```
    ch := make(chan int, 1)

```
通过上述代码获得 channel 变量，我们可以对它执行发送与接收的操作。
* 只接收的 channel
```
       ch := make(<-chan int, 1)

```
通过上述代码获得 channel 变量，我们只能对它进行接收操作。
* 只发送的 channel
```
       ch := make(chan<- int, 1)
```
通过上述代码获得 channel 变量，我们只能对它进行发送操作。



通常只发送 channel 类型和只接收 channel 类型，会被用作函数的参数类型或返回值：
```
func send(ch chan<- int) {
   ch <- 1
}

func recv(ch <-chan int) {
   <-ch
}

```

### channel 的关闭
通过内置函   close(c chan<- Type)，可以对 channel 进行关闭。
* 在发送端关闭 channel

* 在 channel 关闭之后，将不能对 channel 执行发送操作，否则会发生 panic，提示 channel 已关闭。
```
    func main() {
       ch := make(chan int, 5)
       ch <- 1
       close(ch)
       ch <- 2 // panic: send on closed channel
    }

```

* 管道 channel 之后，依旧可以对 channel 执行接收操作，如果存在缓冲区的情况下，将会读取缓冲区的数据，如果缓冲区为空，则获取到的值为 channel 对应类型的零值
```
    import "fmt"

    func main() {
       ch := make(chan int, 5)
       ch <- 1
       close(ch)
       fmt.Println(<-ch) // 1
       n, ok := <-ch
       fmt.Println(n)  // 0
       fmt.Println(ok) // false
    }

```

* 如果通过 for-range 遍历 channel 时，中途关闭 channel 则会导致 for-range 循环结束。