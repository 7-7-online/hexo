---
title: Redis Key 相关操作命令
date: 2024-03-02 23:28:07
tags: [redis]
categories: 
- redis
---

### Key 相关操作命令
> 1、keys
```
格式：KEYS pattern
功能：查找所有符合给定模式 pattern 的 key，pattern 为正则表达式。
说明：KEYS 的速度非常快，但在一个大的数据库中使用它可能会阻塞当前服务器的服务。所以生产环境中一般不使用该命令，而使用 scan 命令代替。
```
![玖涯](/img/redis/redis2/1.png)
> 2、exists
```
格式：EXISTS key
功能：检查给定 key 是否存在。
说明：若 key 存在，返回 1 ，否则返回 0 。
```
![玖涯](/img/redis/redis2/2.png)

> 3、del
```
格式：DEL key [key …]
功能：删除给定的一个或多个 key 。不存在的 key 会被忽略。
说明：返回被删除 key 的数量。
```
![玖涯](/img/redis/redis2/3.png)

> 4、rename
```
格式：RENAME key newkey
功能：将 key 改名为 newkey。
说明：当 key 和 newkey 相同，或者 key 不存在时，返回一个错误。当 newkey 已经存在时， RENAME 命令将覆盖旧值。改名成功时提示 OK ，失败时候返回一个错误。
```
![玖涯](/img/redis/redis2/4.png)

> 5、move
```
格式：MOVE key db
功能：将当前数据库的 key 移动到给定的数据库 db 当中。
说明：如果当前数据库(源数据库)和给定数据库(目标数据库)有相同名字的给定 key ，或者 key 不存在于当前数据库，那么 MOVE 没有任何效果。移动成功返回 1 ，失败则返回 0 。
```
![玖涯](/img/redis/redis2/5.png)

> 6、type
```
格式：TYPE key
功能：返回 key 所储存的值的类型。
说明：返回值有以下六种
​ * none (key 不存在)
​ * string (字符串)
​ * list (列表)
​ * set (集合)
​ * zset (有序集)
​ * hash (哈希表)
```
> 7、expire/pexpire
```
格式：EXPIRE key seconds
功能：为给定 key 设置生存时间。当 key 过期时(生存时间为 0)，它会被自动删除。 expire 的时间单位为秒，pexpire 的时间单位为毫秒。在 Redis 中，带有生存时间的 key被称为“易失”(volatile)。
说明：生存时间设置成功返回 1。若 key 不存在时返回 0 。rename 操作不会改变 key的生存时间。
```
> 8、ttl/pttl
```
格式：TTL key
功能：TTL, time to live，返回给定 key 的剩余生存时间。
说明：其返回值存在三种可能：
​ * 当 key 不存在时，返回 -2 。
​ * 当 key 存在但没有设置剩余生存时间时，返回 -1 。
 * 否则，返回 key 的剩余生存时间。ttl 命令返回的时间单位为秒，而 pttl 命令返回的时间单位为毫秒。
```
![玖涯](/img/redis/redis2/8.png)

> 9、persist
```
格式：PERSIST key
功能：去除给定 key 的生存时间，将这个 key 从“易失的”转换成“持久的”。
说明：当生存时间移除成功时，返回 1；若 key 不存在或 key 没有设置生存时间，则返回 0。
```
![玖涯](/img/redis/redis2/9.png)

> 10、randomkey
```
格式：RANDOMKEY
功能：从当前数据库中随机返回(不删除)一个 key。
说明：当数据库不为空时，返回一个 key。当数据库为空时，返回 nil。
```
![玖涯](/img/redis/redis2/10.png)

> 11、scan

[博客主页地址](https://blog.csdn.net/m0_47015897/article/details/130045912)