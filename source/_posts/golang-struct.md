---
title: golang struct json tag的使用及深入理解
date: 2023-09-20 17:15:23
tags: [go]
categories: 
- 编程
---

### 1.tag格式说明
struct json tag主要在struct与json数据转换的过程(Marshal/Unmarshal)中使用。

json的tag格式如下：

```
Key type  `json:"name,opt1,opt2,opts..."`
```

我们先介绍下源码文档中提供的几种使用方式：

因Marshal与Unmarshal是相反的过程，两者规则是一致的，以下介绍中仅说明了Marshal时的处理。
#### （1）不指定tag

> Field int // “Filed”:0

##### 不指定tag，默认使用变量名称。转换为json时，key为Filed。

#### （2）直接忽略
> Field int json:"-" //注意：必须为"-"，不能带有opts
>

转换时不处理。

#### （3）指定key名
> Field int json:"myName" // “myName”:0
>
##### 转换为json时，key为myName

#### （4）"omitempty"零值忽略
> Field int json:",omitempty"
>

##### 转换为json时，值为零值则忽略，否则key为myName

#### （5）指定key且零值忽略
> Field int json:"myName,omitempty"
>
##### 转换为json时，值为零值则忽略，否则key为myName

#### （6）指定key为"-"
> Field int json:"-," // “-”:0
>
##### 此项与忽略的区别在于多了个”,“。

#### （7）“string” opt

以上提到的用法都是常见的，这个比较特殊。

"string"仅适用于字符串、浮点、整数或布尔类型，表示的意思是：将字段的值转换为字符串；解析时，则是将字符串解析为指定的类型。主要用于与javascript通信时数据的转换。

注意：
仅且仅有"string"，没有int、number之类的opt。即带"string" opt的字段，编码时仅能将字符串、浮点、整数或布尔类型转换为string类型，反之则不然；解码时可以将string转换为其他类型，反之不然。因为"string"有限制

> Int64String int64 json:",string" // “Int64String”:“0”
>

“string” opt的使用可以在Marshal/Unmarshal时自动进行数据类型的转换，减少了手动数据转换的麻烦，但是一定要注意使用的范围，对不满足的类型使用，是会报错的。

猜下对string使用"string" opt的结果会是如何呢？
> Int64String string json:",string"
>

<font face="微软雅黑" >微软雅黑字体</font>
<font face="黑体" >黑体</font>
<font size=3 >3号字</font>
<font size=4 >4号字</font>
<font color=#FF0000 >红色</font>
<font color=#008000 >绿色</font>
<font color=#0000FF >蓝色</font>
