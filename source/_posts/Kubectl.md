---
title: Kubectl 命令
date: 2024-07-31 22:53:21
tags: [linux,k8s]
categories: 
- k8s
---



1. 获取资源
    ```
    kubectl get nodes # 获取集群节点
    kubectl get pods # 获取所有pods
    kubectl get pods -o wide # 列出所有 pod 并显示详细信息
    kubectl get svc # 获取所有服务（显示了服务名称，类型，集群ip，端口，时间等信息）
    kubectl get deployment # 获取所有部署
    kubectl get services # 列出所有密钥
    kubectl describe pods my-pod # 使用详细输出来描述命令
    kubectl get ns # 查看命名空间
    
    参数选项：
    -c, --container="": 容器名。
    -f, --follow[=false]: 指定是否持续输出日志（实时日志）
    
    ```
2. 创建资源
   ```
   kubectl create -f ./my-manifest.yaml # 使用配置文件创建资源
   kubectl create -f ./my1.yaml -f ./my2.yaml # 使用多个文件创建多个资源
   kubectl create -f ./dir # 使用目录下的所有配置文件来创建资源
   kubectl create pod my-pod --image=myimage # 使用命令行参数创建pod
   ```
3. 更新资源
   ```
    kubectl apply -f ./my-manifest.yaml # 更新资源
   ```
4. 删除资源
   ```
    kubectl delete pod my-pod # 删除pod
    kubectl delete deployment my-deployment # 删除部署
    kubectl delete -f ./pod.yml # 使用 pod.yml 中指定的资源类型和名称删除 pod
    kubectl delete pods --all # 删除所有 pod
   ```
5. 描述资源
   ```
    kubectl describe nodes my-node # 描述节点
    kubectl describe pods my-pod # 描述pod
   ```
6. 执行容器内的命令
   ```
   kubectl exec my-pod -- ls / # 在容器中执行命令
   kubectl exec -it nginx-deployment-58d6d6ccb8-lc5fp bash # 进入nginx容器，执行一些命令操作
   命令选项：
   -i, --stdin[=false]: 将控制台输入发送到容器
   -t, --tty[=false]: 将标准输入控制台作为容器的控制台输入
   ```
7. 获取pod日志
   ```
   kubectl logs my-pod # 获取pod的日志
   kubectl logs -f my-pod  # 流式输出 pod 的日志（实时日志数据）
   kubectl logs --tail=20 nginx # 仅输出 pod nginx容器 中最后/最近的 20 条日志
   kubectl logs --since=1h nginx # 输出 pod nginx 中最近一小时内产生的所有日志
   kubectl logs --since=2023-05-20T10:00:00 my-pod  # 查看名为 my-pod 的 pod 在 2023 年 5 月 20 日 10 点之后的日志
   kubectl logs --since="2022-01-01T00:00:00Z" --until="2022-01-02T00:00:00Z" my-pod
   kubectl logs -f 服务名 -n 命名空间 | grep '<要搜索的内容>'
   ```
8.  标签操作
   ```
    kubectl label nodes my-node key=value # 添加标签
    kubectl label nodes my-node key- # 删除标签
   ```
9.  扩缩容
   ```
    kubectl scale deployment my-deployment --replicas=3 # 扩容
    kubectl scale deployment my-deployment --replicas=1 # 缩容
   ```
10. 更新资源
   ```
    kubectl set image deployment/my-deployment my-container=myimage:mytag # 更新资源的镜像
   ```
11. 配置文件和json输出
   ```
    kubectl get pods -o wide # 获取pods并以宽格式输出
    kubectl get pods -o json # 获取pods并以json格式输出
   ```
12. 使用kubectl port-forward
   ```
    kubectl port-forward pod/my-pod 8888:80 # 将本地端口转发到pod端口
   ```
13. 使用kubectl proxy
   ```
    kubectl proxy # 运行代理
   ```
14. 使用kubectl replace
   ```
    kubectl replace -f ./my-deployment.yaml # 使用新的配置文件替换资源
   ```
15. 使用kubectl version
   ```
    kubectl version # 输出客户端和服务器版本信息
   ```
16. 使用kubectl autoscale
   ```
    kubectl autoscale deployment my-deployment --min=2 --max=5 # 自动扩缩容
   ```
17. 使用kubectl rollout
   ```
    kubectl rollout status deployment/my-deployment # 查看部署状态
    kubectl rollout history deployment/my-deployment # 查看部署历史
    kubectl rollout undo deployment/my-deployment # 回滚部署
   ```
18. 使用kubectl cordon, uncordon, drain
   ```
    kubectl cordon my-node
   ```
19.  k8s查看认证信息
    ```
    kubectl get secret -n <命名空间>
    ```
20.  k8s创建私有镜像仓库认证信息
    ```
    kubectl create secret docker-registry <认证名称> --docker-server=<Docker镜像仓库地址> --docker-username='<访问Docker镜像仓库的用户名>' --docker-password='<用户名的帐号密码>' -n <命名空间>
    ```
21.  ss