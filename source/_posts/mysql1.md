---
title: Mysql 基本操作命令行
date: 2024-03-01 14:04:48
tags: [mysql]
categories: 
- mysql
---
## Mysql 基本操作命令行
1. 登录MySQL数据库：
```
基本形式如下
mysql -h主机地址 -u用户名 －p用户密码

连接到本机上的 MySQL
mysql -u root -p，回车后提示你输密码

连接到远程主机上的 MySQL
mysql -h110.110.110.110 -u root -pabcd123

```
2. 显示所有数据库：
```
SHOW DATABASES;
```
3. 创建新数据库：
```
CREATE DATABASE database_name;
```
4. 选择数据库：
```
USE database_name;
```
5. 显示当前数据库中的所有表：
```
SHOW TABLES;
```
6. 创建新表：
```
CREATE TABLE table_name (column_name column_type);
```
7. 查看表结构：
```
DESCRIBE table_name;
```
或者
```
SHOW COLUMNS FROM table_name;
```
9. 插入数据：
```
INSERT INTO table_name (column1, column2) VALUES (value1, value2);
```
10. 查询数据：
```
SELECT * FROM table_name;
```
11. 更新数据：
```
UPDATE table_name SET column_name = 'new_value' WHERE some_column = 'some_value';
```
12. 删除数据：
```
DROP TABLE table_name;
```
13. 删除表：
```
DROP TABLE table_name;
```
14. 删除数据库：
```
DROP DATABASE database_name;
```

[博客主页地址](https://blog.csdn.net/qq_45922256/article/details/133811891)
[博客主页地址2](https://blog.csdn.net/m0_53882348/article/details/130046194)
[博客主页地址3](https://www.cnblogs.com/cherishthepresent/p/17896214.html)