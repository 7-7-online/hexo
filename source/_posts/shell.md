---
title: sh可执行脚本命令
date: 2023-08-13 21:27:08
tags: [shell]
---

#### 一、创建.sh文件
```
vi hello.sh
```
#### 二、编写脚本内容
```
#!/bin/bash
echo "Hello World!"
```
#### 三、添加执行权限
```
chmod +x hello.sh
或者
chmod 755 hello.sh
```
#### 四、执行脚本
```
./hello.sh
```