---
title: Go语言位运算
date: 2024-03-18 14:39:08
tags: [Go语言位运算]
categories: 
- go
---

#### 在 Go 语言中支持以下几种操作位的方式：
* &      位与
* |      位或
* ^      异或
* &^     与非
* <<     左移/右移
  

#### 举例：
##### 1.使用&来判断一个数字是奇数还是偶数：

```
import (
    "fmt"
    "math/rand"
)
func main() {
    for x := 0; x < 100; x++ {
        num := rand.Int()
        if num&1 == 1 {
            fmt.Printf("%d is odd\n", num)
        } else {
            fmt.Printf("%d is even\n", num)
        }
    }
}
```

##### 2.使用|来设置位数：

```
func main() {
    var a uint8 = 0
    a |= 196
    fmt.Printf("%b", a)
}
// prints 11000100
          ^^   ^
```

##### 3.使用^来判断两个数字是否为同号：

```
func main() {
    a, b := -12, 25
    fmt.Println("a and b have same sign?", (a ^ b) >= 0)
}
```

##### 4.使用^来作为非操作：

```
func main() {
    var a byte = 0x0F
    fmt.Printf("%08b\n", a)
    fmt.Printf("%08b\n", ^a)
}
 
// prints
00001111     // var a
11110000     // ^a
```

##### 5.使用&^来清空位：

```
func main() {
    var a byte = 0xAB
    fmt.Printf("%08b\n", a)
    a &^= 0x0F
    fmt.Printf("%08b\n", a)
}
// prints:
10101011
10100000
```

##### 6.使用|和位移来设置位值：

```
func main() {
    var a int8 = 8
    fmt.Printf("%08b\n", a)
    a = a | (1<<2)
    fmt.Printf("%08b\n", a)
}
// prints:
00001000
00001100
```

##### 7.使用&和位移来测试某一位是否置1：

```
func main() {
    var a int8 = 12
    if a&(1<<2) != 0 {
        fmt.Println("take action")
    }
}
// prints:
take action
```

##### 8.使用&^和位移运算来给某一位置0：

```
func main() {
    var a int8 = 13 
    fmt.Printf("%04b\n", a)
    a = a &^ (1 << 2)
    fmt.Printf("%04b\n", a)
}
// prints:
1101
1001
```
