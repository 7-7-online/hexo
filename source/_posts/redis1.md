---
title: Redis基本命令
date: 2024-03-01 14:04:55
tags: [redis]
categories: 
- redis
---
### 常识
```
基本key类型：键（key）
基本数据类型：字符串（String）、散列（hash）、列表（list）、集合（set）、有序集合（sorted）
特殊数据类型：位图（bitmaps）、超长日志（hyperloglog）、地理空间（geospatial）
发布订阅命令
事务命令...
```

### 基本命令
0、连接服务端
>  redis-cli -h 127.0.0.1 -p 6379

1、ping（心跳命令）
> 键入`ping命令`，若看到PONG响应，则说明客户端与Redis的连接时正常的。

![玖涯](/img/redis/redis1/1.png)

2、get/set（读写键值命令）
> set key value 会将指定 key-value写入到DB。get key则会读取指定key的value值。

![玖涯](/img/redis/redis1/2.png)

3、select（切换数据库）
> redis默认有 16 个数据库。这个在 Redis Desktop Manager（RDM）图形客户端中可以直 观地看到。默认使用的是 0 号 DB，可以通过 select db 索引来切换 DB。如图，切换到1号DB：

![玖涯](/img/redis/redis1/3.png)

4、dbsize（查看key数量）
> dbsize 命令可以查看当前数据库中 key 的数量。

![玖涯](/img/redis/redis1/4.png)

5、flushdb（删除当前库中所有数据）
> 清楚当前DB中的所有数据，不影响其他DB。

![玖涯](/img/redis/redis1/5.png)

6、flushall（删除所有DB中的数据）
![玖涯](/img/redis/redis1/6.png)

[博客主页地址](https://blog.csdn.net/m0_47015897/article/details/130045912)

