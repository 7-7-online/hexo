---
title: Go reflect 反射包
date: 2023-11-30 09:06:27
tags: [go]
categories: 
- 编程
- 反射包
---


# reflect 反射包
针对反射，Go 提供了 reflect 包，使用这个包里的函数可以在程序运行时获取和更新未知变量的值，操作未知变量的方法等。

* reflect 包核心的两个重要类型：
  * reflect.Type：Type 是一个接口，不同数据类型有着不同的结构体实现。这个接口用于操作变量的类型信息，类型的信息只能读取。
  * reflect.Value：Value 是一个结构体，通过这个结构体可以操作变量的值。
# TypeOf(i) 和 ValueOf(i)
* reflect.TypeOf(i any) Type：获取变量的类型，返回一个 reflect.Type  类型。
* reflect.ValueOf(i any) Value：获取变量的值，返回 reflect.Value  类型，通过 Value 可以对获取变量更多的信息。

### 案例 1：获取变量的类别和类型信息
```
import (
    "fmt"
    "reflect"
)

type User struct {
    Name string
}

func main() {
    user := User{
            Name: "cmy",
    }
    func4Reflect(user)
}

func func4Reflect(data any) {
    typ := reflect.TypeOf(data)
    fmt.Println("类别：", typ.Kind()) // 类别： struct
    fmt.Println("类型：", typ.Name()) // 类型： User
}

```
* 通过 TypeOf() 函数获取 data 的类型信息，然后调用 Kind() 和 Name() 方法分别获取 data 变量的类别和类型信息。
  
*  根据返回结果可知， Kind() 返回的是 Go 的数据类型，而 Name() 返回的是我们自定义的数据类型。
  
*  根据 Kind() 返回值的特点，可以用于判断变量属于 Go 的哪种数据类型，用于类型限制等场景。

### 案例 2：修改基本数据类型变量的值
```
import (
    "fmt"
    "reflect"
)

func main() {
    num1 := 666
    fmt.Println("num1 原值：", num1)
    func4Reflect(&num1)
    fmt.Println("num1 修改后的值：", num1)

    num2 := 0.5
    fmt.Println("num2 原值：", num2)
    func4Reflect(&num2)
    fmt.Println("num2 修改后的值：", num2)

    str := "go"
    fmt.Println("str 原值：", str)
    func4Reflect(&str)
    fmt.Println("str 修改后的值：", str)
}

func func4Reflect(data any) {
    typ := reflect.TypeOf(data)
    val := reflect.ValueOf(data)
    switch typ.Elem().Kind() {
    case reflect.Int:
            val.Elem().SetInt(888)
    case reflect.Float64:
            val.Elem().SetFloat(3.14)
    case reflect.String:
            val.Elem().SetString("Golang")
    }
}

```

* 通过 ValueOf() 函数获取 data 变量的值信息，然后结合 reflect.Type.Kind() 方法，对不同类型的变量的值进行修改操作（只举三种类型的例子）：

  * int 类型 → 使用 SetInt(val) 方法对值进行修改。

  * float64 → 使用 SetFloat(val) 方法对值进行修改。

  * string 类型 → 使用 SetString(val) 方法对值进行修改。

* data 必须是指针类型，否则无法通过反射修改。

* 由于是指针类型，因此需要调用 Elem() 方法获取到指针指向的变量，才能修改变量的值。

### 案例 3：通过反射获取结构体的字段名、字段类型和字段的值
```
import (
    "fmt"
    "reflect"
)

type User struct {
    Name string
    Age  int
}

func main() {
    user := User{
            Name: "cmy",
            Age:  18,
    }
    func4Reflect(user)
}

func func4Reflect(data any) {
    typ := reflect.TypeOf(data)
    val := reflect.ValueOf(data)
    // 获取结构体字段的数量
    numField := val.NumField()
    for i := 0; i < numField; i++ {
            fmt.Println("字段名称：", typ.Field(i).Name)
            fmt.Println("字段类型：", typ.Field(i).Type.Name())
            fmt.Println("字段值：", val.Field(i).Interface())
            fmt.Println("----------------------------")
    }
}

```
* 首先通过 TypeOf() 和 ValueOf() 获取到结构体的类型信息和值信息。

* 其次通过 Value.NumField() 方法获取到结构体字段的数量。

* 接着遍历结构体的字段，通过 Type.Field(i) 方法，传入索引，获取到对应字段的类型信息，通过 Name 属性获取字段名，Type.Name() 获取字段类型。

* 最后通过 Value.Field(i) 方法，传入索引，获取到对应字段的值信息，通过 Interface() 方法获取字段实际的值。




##### https://blog.csdn.net/weiguang102/article/details/129209213