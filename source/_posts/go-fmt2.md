---
title: Golang + Fmt 格式化字符串
date: 2023-09-20 17:21:40
tags: [linux,go,fmt]
---

#### 下面是一些常见的格式化操作
##### [地址链接](https://blog.csdn.net/GeeCode/article/details/131254780)

| 占位符 | 描述  |  示例  |  返回内容   |
| :----: | :-----: | :------: | :------: |
|  %v |  值的默认格式。当输出结构体时，会添加字段名。   | fmt.Sprintf("%v",d)  |  Music   |
|  %+v  | 与%v相同，但输出结构体时会添加字段名。 |    fmt.Sprintf("%+v",d   |   Math   |
| %#v |值的Go语法表示。输出格式类似于Go语言中的代码。 | fmt.Sprintf("#v",d | Computer |
| %T | 值的类型。 | fmt.Sprintf("%T",d | Computer |
| %t | 布尔值，true或false。 | fmt.Sprintf("%t",d | Computer |
| %d | 十进制整数。 | fmt.Sprintf("%d",d | Computer |
| %b | 二进制整数。 | fmt.Sprintf("%b",d | Computer |
| %o | 八进制整数。 | fmt.Sprintf("%o",d | Computer |
| %x | 十六进制整数，小写字母。 | fmt.Sprintf("%x",d | Computer |
| %X | 十六进制整数，大写字母。 | fmt.Sprintf("%X",d | Computer |
| %f | 浮点数。 | fmt.Sprintf("%f",d | Computer |
| %.2f | 保留两位浮点数。 | fmt.Sprintf("%.2f",d | Computer |
| %e | 科学计数法表示的浮点数，小写字母’e’。 | fmt.Sprintf("%e",d | Computer |
| %E | 科学计数法表示的浮点数，大写字母’E’。 | fmt.Sprintf("%E",d | Computer |
| %s | 字符串。 | fmt.Sprintf("%s",d | Computer |
| %q | 单引号括起来的字符串，由Go语法安全地转义。 | fmt.Sprintf("%q",d | Computer |
| %p | 指针的十六进制表示，前面加上0x。 | fmt.Sprintf("%p",d | Computer |
| %U | Unicode格式，U+1234，等价于"U+%04X"。 | fmt.Sprintf("%U",d | Computer |
| %c | Unicode字符。 | fmt.Sprintf("%c",d | Computer |