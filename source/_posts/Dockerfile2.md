---
title: Dockerfile例子
date: 2024-04-26 16:23:51
tags: [Docker-Dockerfile]
categories:
  - docker
---


## 例子

```
# 第一阶段：构建 ，golang:alpine轻量级 Linux 发行版
FROM golang:alpine as BUILD

#指定镜像创建者信息
MAINTAINER Netcc chen@7-7.online

# 设置工作目录
WORKDIR /home/go

# 将go module设置为on，并指定模块的代理
ENV GO111MODULE=on
ENV GOPROXY=https://goproxy.cn,direct

# 将应用程序的所有文件复制到位于/app中的容器
COPY . /home/go

# 编译go项目，生成二进制文件
# RUN go build -o app .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o app .

# 设置环境变量
ENV HTTP_PORT=6789

# 声明运行容器时应该暴露的端口
EXPOSE 6789

# 第二阶段：运行，生成最终镜像
# 阶段使用alpine:latest作为基础镜像
FROM alpine:latest

# 设置工作目录
WORKDIR /home/go

# 从构建阶段复制二进制文件 todo 静态文件需要拷贝进镜像中，要不然找不到文件
# COPY --from=builder /go/src/app/app /go/src/app/app
COPY --from=BUILD /home/go/app /home/go/app
COPY --from=BUILD /home/go/views /home/go/views
COPY --from=BUILD /home/go/static /home/go/static

# 设置容器启动时运行的命令
CMD ["/home/go/app"]
#ENTRYPOINT ["go","run","main.go"]
```