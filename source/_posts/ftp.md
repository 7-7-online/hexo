---
title: FTP 介绍
date: 2024-06-12 17:07:48
tags: [ftp]
categories:
  - ftp
---

#### 1. FTP
1. **使用命令行连接ftp服务器**
   1. `ftp端口是默认的21端口时`
      
      > 格式：ftp 服务器IP地址
      ```
        [root@uhwlgegh1v326i ~]# ftp 172.31.0.15
        Connected to 172.31.0.15 (172.31.0.15).
        220 (vsFTPd 3.0.2)
        Name (172.31.0.15:root): linchunpeng		# 账号
        331 Please specify the password.			# 密码
        Password:
        230 Login successful.
        Remote system type is UNIX.
        Using binary mode to transfer files.
      ```
   2. `使用指定端口连接ftp服务器`
      
      > 格式：ftp
      > open 服务器IP 端口
      > 或者 sftp -P 端口号 用户名@服务器地址
      ```
         [root@uhwlgegh1v326i ~]# ftp
         ftp> open 172.31.0.15 21
         Connected to 172.31.0.15 (172.31.0.15).
         220 (vsFTPd 3.0.2)
         Name (172.31.0.15:root):  linchunpeng
         331 Please specify the password.
         Password:
         530 Login incorrect.
         Login failed.
         ftp>
      ```
2. **SFTP**
   1. 连接SFTP服务器
      > 第一步：进入终端
      > 第二步: `sftp -P 端口号 用户名@服务器地址`
3. **处理文件**
   1. curl命令
      * curl 查看文件目录
        ```
           curl ftp://username:password@192.168.10.91:221/test/
        ```
        `最后一定要加斜杠“/”，否则报错`
      * curl 上传ftp 文件
        ```
          curl -T /path/to/local/file.txt ftp://username:password@ftp.server.com/path/to/remote/directory/
          格式：curl -T 本地的myfile.txt ftp://ftp账户:ftp密码@ftp.server.com/服务器存放文件目录/
        ```
        请将`/path/to/local/file.txt`替换为你想上传的本地文件的路径，`username`和`password`替换为你的FTP服务器的用户名和密码，`ftp.server.com`替换为你的FTP服务器的域名或IP地址，`/path/to/remote/directory/`替换为远程服务器上的目标目录路径。

        ```
           例子：curl --ftp-create-dirs -T /root/docker-pack-env/pack/linux/xxx.tar.gz.md5 ftp://username:password@192.168.10.91:221/2022/20220226/xxx.gz.md5
        ```
        `--ftp-create-dirs 代表在ftp上自动创建目标文件夹。`
      * curl 下载ftp 文件
        ```
           curl ftp://username:password@192.168.10.91:221/test/20211229/xxx.dat -o /root/xxx.dat
        ```
        `这里用到的参数是 -o，注意参数位置，不在最前面。`
   2. ftp命令
      * 执行以下命令，将本地文件上传至轻量应用服务器中：
        ```
           put local-file [remote-file]

           例如，将本地文件 /home/1.txt 上传到轻量应用服务器。
           put /home/1.txt 1.txt
        ```
      * 执行以下命令，将轻量应用服务器中的文件下载至本地：
        ```
           get [remote-file] [local-file]

           例如，将轻量应用服务器中的 A.txt 文件下载到本地的 /home 目录下。
           get A.txt /home/A.txt
        ```
      


链接：https://blog.csdn.net/netyeaxi/article/details/132676226