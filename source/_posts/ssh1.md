---
title: 通过ssh端口转发实现内网穿透效果
date: 2024-06-26 20:27:15
tags: [linux]
categories:
  - ssh
---

#### 1. 添加ssh远程连接配置(在本地操作)
        ```
# 编辑ssh远程连接配置
vim ~/.ssh/config

# 添加以下配置
Host aaa.internet.company
HostName 10.0.1.23
User root
# 通过指定的密钥免密登录
IdentityFile ~/.ssh/id_ed25519_xxx
# 将本地主机(192.168.2.88)的8000端口映射到远程主机(10.0.1.23)的8000端口, 相当于访问192.168.1.94:8000的流量都转发到10.0.0.23:8000上去
# LocalForward <本地IP>:<本地端口> <远程IP>:<远程端口>
LocalForward 192.168.2.88:8000 10.0.1.23:8000
# 将远程主机(10.0.1.23)的9000端口映射到本地主机(192.168.2.88)的9000端口, 相当于访问10.0.0.23:9000的流量都转发到192.168.1.94:9000上去
# RemoteForward <远程IP>:<远程端口> <本地IP>:<本地端口>
RemoteForward 10.0.1.23:9000 192.168.2.88:9000


        ```

`LocalForward`配置的作用: 让本地机器所在`局域网`内的其他机器可以通过访问本地机器的指定端口来访问远程机器的指定端口(相当于反向内网穿透)

`RemoteForward`配置的作用: 让远程机器所在局域网内的其他机器可以通过访问远程机器的指定端口来访问本地机器的指定端口(相当于内网穿透)

* 连接远程服务器
```
# 连接服务器
ssh aaa.internet.company

```
#### 2. 调整ssh配置(在服务器中操作)
允许SSH服务器监听所有网络接口上的转发端口，而不仅仅是本地回环地址

```
# 编辑文件
vim /etc/ssh/sshd_config
# 找到`GatewayPorts`配置, 放开注释, 并修改值为yes: `GatewayPorts yes`, 不配置只能通过回环地址(localhost)转发
# 重启ssh服务, 使配置生效
systemctl restart sshd

```

#### 3. 开放端口(在服务器中操作)

```
# 查看开放的端口
# firewall-cmd --list-ports
# 开发指定的端口
firewall-cmd --zone=public --add-port=9000/tcp --permanent
firewall-cmd --reload
# 关闭指定的端口
# firewall-cmd --zone=public --remove-port=9000/tcp --permanent
# firewall-cmd --reload

```

#### 5. 开启流量转发(在本地操作)

```
# 前台启动
# ssh -N aaa.internet.company
# 后台启动
ssh -Nf aaa.internet.company

# -N 不执行远程命令，只进行数据转发
# -f 后台运行

```
> 至此, 即可通过服务器IP加端口访问本地服务了, 也可配合nginx反向代理本地服务, 通过IP或域名访问, 无需再开放额外的端口



外部链接：https://blog.csdn.net/weixin_34789114/article/details/135891014