---
title: Go fmt 标准库常用方法的使用
date: 2023-12-01 00:12:24
tags: [go,fmt]
---

# 前言
* Go 语言中的 fmt 标准库是用于格式化输入和输出的库。它提供了一组函数，可以对标准输出、标准错误输出和字符串进行格式化输出。本文将对其常用方法进行介绍。

> 占位符
* 普通占位符
![玖涯](/img/fmt/1.webp)
* 布尔
![玖涯](/img/fmt/2.webp)
* 整数
![玖涯](/img/fmt/3.webp)
* 浮点数
![玖涯](/img/fmt/4.webp)
* 字符串和字节切片
![玖涯](/img/fmt/5.webp)
* 切片
![玖涯](/img/fmt/6.webp)
* 指针
![玖涯](/img/fmt/7.webp)

> fmt 输出函数

![玖涯](/img/fmt/8.webp)
> Fprint、Fprintf、Fprintln
* 
* 
* 
* 
* 

# https://xie.infoq.cn/article/686babd176b57f57b8eb188f7